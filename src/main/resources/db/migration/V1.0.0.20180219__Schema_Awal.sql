CREATE TABLE customer(
  id VARCHAR (36) PRIMARY KEY,
  code VARCHAR (7) UNIQUE NOT NULL,
  counter INT NOT NULL,
  name VARCHAR(100) NOT NULL,
  address VARCHAR (200) NOT NULL,
  telp VARCHAR (20) NOT NULL,
  contact_person VARCHAR (100),
  email VARCHAR (100)
);

CREATE TABLE item(
  id VARCHAR (36) PRIMARY KEY,
  code VARCHAR (7) UNIQUE NOT NULL,
  counter INT,
  name VARCHAR(200) NOT NULL,
  price DECIMAL(11,2) NOT NULL
);

CREATE TABLE sales(
  id VARCHAR(36) PRIMARY KEY,
  document_number VARCHAR(17) UNIQUE NOT NULL,
  counter INT,
  sales_date TIMESTAMP NOT NULL,
  customer_id VARCHAR(36) REFERENCES customer
);

CREATE TABLE sales_detail(
  id VARCHAR (36) PRIMARY KEY,
  sales_id VARCHAR (36) REFERENCES sales,
  item_id VARCHAR (36) REFERENCES item,
  qty INT,
  price DECIMAL(15,2),
  total DECIMAL (17,2)
);