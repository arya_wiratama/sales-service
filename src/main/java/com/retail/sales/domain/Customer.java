package com.retail.sales.domain;

import lombok.Builder;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Builder
public class Customer {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @Column(length = 7, unique = true, nullable = false)
    @NotNull
    private String code;

    @NotNull
    private int counter;

    @Column(length = 100, nullable = false)
    @NotNull
    private String name;

    @Column(length = 200, nullable = false)
    @NotNull
    private String address;

    @Column(length = 20)
    private String telp;

    @Column(length = 100, nullable = false)
    @NotNull
    private String contactPerson;

    @Column(length = 100)
    private String email;
}
