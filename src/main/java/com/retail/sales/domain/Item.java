package com.retail.sales.domain;

import lombok.Builder;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Data
@Builder
public class Item {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    public String id;

    @Column(length = 7, unique = true, nullable = false)
    @NotNull
    private String code;

    @NotNull
    private int counter;

    @Column(length = 200, nullable = false)
    @NotNull
    private String name;

    @Column(length = 11, precision = 2)
    @NotNull
    private BigDecimal price;
}
