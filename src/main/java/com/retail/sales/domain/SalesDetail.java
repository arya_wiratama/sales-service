package com.retail.sales.domain;

import lombok.Builder;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@Builder
@Table(name = "sales_detail")
public class SalesDetail {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_id", nullable = false)
    private Item item;

    @ManyToOne
    @JoinColumn(name = "sales_id", nullable = false)
    private Sales sales;

    @Column(length = 4)
    private int qty;

    @Column(length = 15, precision = 2)
    private BigDecimal price;

    @Column(length = 15, precision = 2)
    private BigDecimal total;
}
