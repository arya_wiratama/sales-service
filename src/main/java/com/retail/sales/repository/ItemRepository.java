package com.retail.sales.repository;

import com.retail.sales.domain.Item;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ItemRepository extends PagingAndSortingRepository<Item, String> {
}
