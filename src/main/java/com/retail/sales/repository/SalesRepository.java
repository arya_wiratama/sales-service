package com.retail.sales.repository;

import com.retail.sales.domain.Sales;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SalesRepository extends PagingAndSortingRepository<Sales, String> {

    Sales findFirstByOrderByCounterDesc();
}
