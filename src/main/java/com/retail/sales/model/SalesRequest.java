package com.retail.sales.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class SalesRequest {

    @JsonProperty("customer_Id")
    private String customerId;

    @JsonProperty("sales_details")
    private List<SalesDetailRequest> salesDetails;
}
