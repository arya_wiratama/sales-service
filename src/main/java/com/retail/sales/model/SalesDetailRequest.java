package com.retail.sales.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SalesDetailRequest {

    @JsonProperty("item_id")
    private String itemId;
    private int qty;
}
