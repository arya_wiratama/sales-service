package com.retail.sales.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
public class SalesResponse {

    private String id;
    @JsonProperty("document_number")
    private String documentNumber;

    @JsonProperty("sales_date")
    @JsonFormat(pattern = "dd/MM/yyyy hh:mm")
    private Date salesDate;

    @JsonProperty("customer_id")
    private String customerId;

    @JsonProperty("customer_name")
    private String customerName;

    @JsonProperty("sales_details")
    private List<SalesDetailReponse> salesDetails;
}
