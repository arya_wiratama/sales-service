package com.retail.sales.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
public class SalesDetailReponse {

    private String id;

    private String itemId;

    private String itemName;

    private int qty;

    private BigDecimal price;

    private BigDecimal total;
}
