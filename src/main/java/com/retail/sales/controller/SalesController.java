package com.retail.sales.controller;

import com.retail.sales.model.SalesRequest;
import com.retail.sales.model.SalesResponse;
import com.retail.sales.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("api/v1/sales")
public class SalesController {
    @Autowired
    private SalesService salesService;

    @PostMapping
    public ResponseEntity<SalesResponse> saveSales(@Valid @RequestBody SalesRequest request)throws Exception{
        SalesResponse salesResponse = salesService.saveSales(request);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(salesResponse).toUri();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(uri);
        return new ResponseEntity<>(salesResponse, httpHeaders, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<SalesResponse> editSales(@PathVariable("id") String id, @Valid @RequestBody SalesRequest request)throws Exception{
        SalesResponse salesResponse = salesService.editSales(id, request);
        return new ResponseEntity<>(salesResponse, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SalesResponse> findSalesById(@PathVariable("id")String id)throws Exception{
        return new ResponseEntity<>(salesService.findById(id), new HttpHeaders(), HttpStatus.OK);
    }

    public ResponseEntity<Page<SalesResponse>> findAllSales(@RequestParam(name = "page") int page,
                                                            @RequestParam(name = "size")int size)throws Exception{
        return new ResponseEntity<>(salesService.findAllSales(new PageRequest(page,size)), new HttpHeaders(), HttpStatus.OK);
    }
}