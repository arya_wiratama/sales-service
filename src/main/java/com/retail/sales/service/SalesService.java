package com.retail.sales.service;

import com.retail.sales.domain.Customer;
import com.retail.sales.domain.Item;
import com.retail.sales.domain.Sales;
import com.retail.sales.domain.SalesDetail;
import com.retail.sales.model.SalesDetailReponse;
import com.retail.sales.model.SalesDetailRequest;
import com.retail.sales.model.SalesRequest;
import com.retail.sales.model.SalesResponse;
import com.retail.sales.repository.CustomerRepository;
import com.retail.sales.repository.ItemRepository;
import com.retail.sales.repository.SalesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SalesService {

    private ItemRepository itemRepository;
    private CustomerRepository customerRepository;
    private SalesRepository salesRepository;
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");

    @Autowired
    public SalesService(ItemRepository itemRepository, CustomerRepository customerRepository, SalesRepository salesRepository) {
        this.itemRepository = itemRepository;
        this.customerRepository = customerRepository;
        this.salesRepository = salesRepository;
    }

    @Transactional
    public SalesResponse saveSales(SalesRequest request) throws Exception{
        Sales sales = generateSales(request);
        sales = salesRepository.save(sales);
        return generateSalesResponse(sales);
    }

    @Transactional
    public SalesResponse editSales(String id, SalesRequest request)throws Exception{
        Sales oldSales  = salesRepository.findOne(id);
        Sales sales = generateSales(oldSales, request);
        sales = salesRepository.save(sales);
        return generateSalesResponse(sales);
    }

    public Page<SalesResponse> findAllSales(Pageable pageable)throws Exception{
        return salesRepository.findAll(pageable).map(sales -> generateSalesResponse(sales));
    }

    public SalesResponse findById(String id){
        Sales sales = salesRepository.findOne(id);
        return generateSalesResponse(sales);
    }

    private SalesResponse generateSalesResponse(Sales sales) {
        Customer customer = customerRepository.findOne(sales.getCustomer().getId());
        return SalesResponse.builder()
                .id(sales.getId())
                .customerId(customer.getId())
                .customerName(customer.getName())
                .documentNumber(sales.getDocumentNumber())
                .salesDate(sales.getSalesDate())
                .salesDetails(
                        sales.getSalesDetails()
                        .stream()
                        .map(detail -> generateSalesDetailResponse(detail))
                        .collect(Collectors.toList()))
                .build();
    }

    private SalesDetailReponse generateSalesDetailResponse(SalesDetail detail) {
        Item item = itemRepository.findOne(detail.getItem().getId());
        return SalesDetailReponse.builder()
                .id(detail.getId())
                .itemId(item.getId())
                .itemName(item.getName())
                .price(detail.getPrice())
                .qty(detail.getQty())
                .total(detail.getTotal())
                .build();
    }

    private Sales generateSales(SalesRequest request) {
        Customer customer = customerRepository.findOne(request.getCustomerId());
        Optional<Sales> lastSales = Optional.of(salesRepository.findFirstByOrderByCounterDesc());
        int counter = lastSales.isPresent() ? lastSales.get().getCounter() : 1;
        String documentNumber = "INV/"+dateFormatter.format(new Date())+"/"+String.format("%03d", counter);
        Sales sales = Sales.builder()
                .counter(counter)
                .documentNumber(documentNumber)
                .customer(customer)
                .salesDate(new Date())
                .build();
        sales.setSalesDetails(
                request.getSalesDetails()
                        .stream()
                        .map(detail -> generateSalesDetail(detail, sales))
                        .collect(Collectors.toList())
        );
        return sales;
    }

    private Sales generateSales(Sales oldsales, SalesRequest request) {
        Customer customer = customerRepository.findOne(request.getCustomerId());
        Sales sales = Sales.builder()
                .id(oldsales.getId())
                .counter(oldsales.getCounter())
                .documentNumber(oldsales.getDocumentNumber())
                .customer(customer)
                .salesDate(oldsales.getSalesDate())
                .build();
        sales.setSalesDetails(
                request.getSalesDetails()
                        .stream()
                        .map(detail -> generateSalesDetail(detail, sales))
                        .collect(Collectors.toList())
        );
        return sales;
    }

    private SalesDetail generateSalesDetail(SalesDetailRequest detail, Sales sales) {
        Item item = itemRepository.findOne(detail.getItemId());
        BigDecimal total = item.getPrice().multiply(BigDecimal.valueOf(detail.getQty()));
        return SalesDetail.builder()
                .item(item)
                .price(item.getPrice())
                .qty(detail.getQty())
                .total(total)
                .sales(sales)
                .build();
    }
}
